<?php

$url = 'https://api.thingspeak.com/channels/1823180/feed/last.json?api_key=F5NC21Q7QKD7GGZH';
$data = file_get_contents($url);
$characters = json_decode($data, true);

$field1=$characters['field1'];
$field2=$characters['field2'];
$field3=$characters['field3'];
$field4=$characters['field4'];
$field5=$characters['field5'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Application form</title>
    <link type="text/css" rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="MDB/css/mdb.min.css">
    <link type="text/css" rel="stylesheet" href="MDB/css/style.css">
</head>
<body>


<nav class="navbar navbar-expand-lg navbar-dark indigo">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="#">Water Quality</a>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

</nav>

<div class="container">
    <div class="row">
        <!-- Form controls -->
        <div class="col-12" >
            <div class="panel-body">
                <div class="footer pt-3 mdb-color mt-5"></div>
                <form class="col-12 mt-5" action="" method="post" enctype="multipart/form-data" >
                    <div class="row">
                        <div class="col-4">
                            <div class="md-form">
                                <input
                                        value="<?php echo $field1 ?>"
                                        name="field1"
                                        type="text" id="Form-email2" class="form-control">
                                <label style="margin-top: -50px;">Temp</label>

                            </div>
                        </div>
                        <div class="col-4">
                            <div class="md-form">
                                <input
                                        value="<?php echo $field2 ?>"
                                        name="field2"
                                        type="text" class="form-control">
                                <label style="margin-top: -50px;">PH</label>

                            </div>
                        </div>
                         <div class="col-4">
                            <div class="md-form">
                                <input
                                        value="<?php echo $field3 ?>"
                                        name="field3"
                                        type="text" class="form-control">
                                <label style="margin-top: -50px;">Conduct</label>

                            </div>
                        </div>
                        <div class="col-4">
                            <div class="md-form">
                                <input
                                        value="<?php echo $field4 ?>"
                                        name="field4"
                                        type="text" class="form-control">
                                <label style="margin-top: -50px;">Solidity</label>

                            </div>
                        </div>
                        <div class="col-4">
                            <div class="md-form">
                                <input
                                        value="<?php echo $field5 ?>"
                                        name="field5"
                                        type="text" class="form-control">
                                <label style="margin-top: -50px;">Tur</label>

                            </div>
                        </div>
                        
                    </div>
                   
                    


                </form>
                <div class="footer pt-3 mdb-color mt-5"></div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $('#message').show().delay(3000).fadeOut(1500);
</script>
</body>
</html>
